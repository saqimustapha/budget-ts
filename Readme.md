 ### Présentation de l'application Budget-ts:
Est une application de gestion de budget en front se basant sur HTML/CSS et Typescript.
### Maquette de l'application : 
![maquette](Gestion%20de%20budget.png)
### Fonctionnalités principales :

L'application permet de gérer un budget en indiquant: 

- Le solde actuel
- Les entrées d'argent : Revenus
- Les sorties d'argent : Dépenses
- Historique des transactions
- Ajouter/supprimer une transaction

### Fonctionnalité Additionnelle :

Elle permet de sauvegarder le budget actuel en localStorage

### Piste d'amélioration : 

Insérer un calendier pour afficher les dates des transactions.

